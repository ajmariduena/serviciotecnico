<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Servicio Técnico Online Comandato</title>
        <meta name="description" content="Centro Online de atención, para servicio técnico y repuestos.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,800" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <svg id="svg">
            <symbol id="right" viewBox="0 0 24.5 45.8">
                <path d="M2.6,45.4c7.2-6.9,14.3-13.9,21.5-20.8c0.6-0.6,0.6-1.5,0-2.1C16.9,15.1,9.7,7.8,2.6,0.4
                C1.2-0.9-0.9,1.2,0.5,2.6c7.2,7.3,14.3,14.6,21.5,22c0-0.7,0-1.4,0-2.1C14.8,29.4,7.6,36.3,0.5,43.2C-0.9,44.6,1.2,46.7,2.6,45.4
                L2.6,45.4z"/>
            </symbol>
            <symbol id="left" viewBox="0 0 24.5 45.8">
                <path d="M21.9,0.4C14.8,7.4,7.6,14.3,0.4,21.3c-0.6,0.6-0.6,1.5,0,2.1c7.2,7.3,14.3,14.6,21.5,22
                c1.4,1.4,3.5-0.7,2.1-2.1c-7.2-7.3-14.3-14.6-21.5-22c0,0.7,0,1.4,0,2.1C9.7,16.4,16.9,9.5,24.1,2.6C25.4,1.2,23.3-0.9,21.9,0.4
                L21.9,0.4z"/>
            </symbol>
            <symbol id="down" viewBox="0 0 18 28">
                <path d="M16.797 13.5c0 0.125-0.063 0.266-0.156 0.359l-7.281 7.281c-0.094 0.094-0.234 0.156-0.359 0.156s-0.266-0.063-0.359-0.156l-7.281-7.281c-0.094-0.094-0.156-0.234-0.156-0.359s0.063-0.266 0.156-0.359l0.781-0.781c0.094-0.094 0.219-0.156 0.359-0.156 0.125 0 0.266 0.063 0.359 0.156l6.141 6.141 6.141-6.141c0.094-0.094 0.234-0.156 0.359-0.156s0.266 0.063 0.359 0.156l0.781 0.781c0.094 0.094 0.156 0.234 0.156 0.359zM16.797 7.5c0 0.125-0.063 0.266-0.156 0.359l-7.281 7.281c-0.094 0.094-0.234 0.156-0.359 0.156s-0.266-0.063-0.359-0.156l-7.281-7.281c-0.094-0.094-0.156-0.234-0.156-0.359s0.063-0.266 0.156-0.359l0.781-0.781c0.094-0.094 0.219-0.156 0.359-0.156 0.125 0 0.266 0.063 0.359 0.156l6.141 6.141 6.141-6.141c0.094-0.094 0.234-0.156 0.359-0.156s0.266 0.063 0.359 0.156l0.781 0.781c0.094 0.094 0.156 0.234 0.156 0.359z"></path>
            </symbol>
        </svg>
        <div id="app">
            <header class="header">
                <div class="container">
                    <figure><img src="{{ asset('images/servicio-tecnico-logo.svg') }}" alt=""></figure>
                    <figure><img src="{{ asset('images/comandato-logo.svg') }}" alt=""></figure>
                </div>
            </header>

            <div class="hero_container" style="background-image: url({{ asset('images/back.svg') }});">
                <div class="hero cycle-slideshow"
                    data-cycle-fx="scrollHorz"
                    data-cycle-swipe=true
                    data-cycle-log="false"
                    data-cycle-timeout="0"
                    data-cycle-slides="> div.hero_item"
                    data-cycle-prev=".cycle-prev"
                    data-cycle-next=".cycle-next"
                >
                    <div class="cycle-pager"></div>
                    <div class="hero_item">
                        <div class="hero_content">
                            <div class="item"><img src="{{ asset('images/suzuki-vstrom-650.png') }}" alt=""></div>
                            <div class="item">
                                <img src="{{ asset('images/suzuki-logo.svg') }}" alt="">
                                <p>Servicio Técnico Especializado <i>te recomienda</i> realizar el cambio de aceite de tu moto Suzuki a tiempo</p>
                            </div>
                            <div class="circle">
                                <svg x="0px" y="0px" width="35.5px" height="65.2px" viewBox="0 0 35.5 65.2" style="enable-background:new 0 0 35.5 65.2;" xml:space="preserve">
                                    <polygon style="fill:#005196;" points="35.5,35.6 19.3,28.8 31.5,0 0,29.5 16.2,36.4 4,65.2 "/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="hero_item">
                        <div class="hero_content">
                            <div class="item"><img src="{{ asset('images/aire-acondicionado.png') }}" alt=""></div>
                            <div class="item">
                                <img src="{{ asset('images/comandato-logo.svg') }}" alt="">
                                <p>¡En esta temporada de calor! Dale un buen mantenimiento a tu split con nuestro Servicio Técnico especializado.</p>
                            </div>
                            <div class="circle">
                                <svg x="0px" y="0px" width="35.5px" height="65.2px" viewBox="0 0 35.5 65.2" style="enable-background:new 0 0 35.5 65.2;" xml:space="preserve">
                                    <polygon style="fill:#005196;" points="35.5,35.6 19.3,28.8 31.5,0 0,29.5 16.2,36.4 4,65.2 "/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow_wrapper">
                    <div class="cycle-prev"><svg><use xlink:href="#left" /></svg></div>
                    <div class="cycle-next"><svg><use xlink:href="#right" /></svg></div>
                </div>
            </div>

            <section class="contact">
                <h1>Ingresa tus datos para comunicarte con Servicio Técnico Comandato</h1>
                <hr>
                <div class="succes_message" v-if="success">
                    <h2>Gracias por comunicarte con nosotros</h2>
                    <p>Responderemos tu solicitud en un plazo de máximo 24 horas para proceder a brindarte la ayuda especializada que estás buscando.</p>
                </div>
                <div class="tab_links" v-if="!success">
                    <span>Selecciona con que Servicio Técnico deseas comunicarte:</span>
                    <nav>
                        <li @click="setType('brands')" v-bind:class="{ 'tab_link-active': type == 'brands' }"><img src="{{ asset('images/servicio-tecnico-marcas-comandato.svg')}}" alt=""></li>
                        <li @click="setType('suzuki')" v-bind:class="{ 'tab_link-active': type == 'suzuki' }"><img src="{{ asset('images/servicio-tecnico-suzuki-comandato.svg')}}" alt=""></li>
                        <li @click="setType('parts')" v-bind:class="{ 'tab_link-active': type == 'parts' }"><img src="{{ asset('images/respuestos-suzuki-comandato.svg')}}" alt=""></li>
                    </nav>
                </div>
                 <div class="form" v-if="!success">
                    <div class="errors" v-show="errors">
                        <span v-for="error in errors">@{{ error[0] }}</span>
                    </div>
                    <input type="text" placeholder="Nombre y Apellido" v-model="names">
                    <input type="text" placeholder="# de Teléfono" v-model="phone">
                    <input type="text" placeholder="# de Cédula" v-model="legal_id">
                    <textarea name="" id="" placeholder="¿En qué podemos ayudarte?" v-model="message"></textarea>
                </div>
                <div class="submit" v-if="!success">  
                    <div class="submit_wrapper">
                        <div class="wow animated fadeInUp"><img clip-path="url(#hexagonal-mask)" src="{{ asset('images/submit-1.png') }}" alt=""></div>
                        <div><a @click.prevent="submit()" href="">@{{ text }}</a><span class="line"></span></div>
                        <div class="wow animated fadeInUp"><img src="{{ asset('images/submit-2.png') }}" alt=""></div>
                    </div>
                </div>
            </section>

            <section class="help" v-bind:class="{ 'help-disabled': success }">
                <h1>¿Cómo te ayudamos?</h1>
                <img class="help-logo" src="{{ asset('images/comandato-logo.svg') }}" alt="">
                <hr>
                <div class="help_grid">
                    <div class="col">
                        <div class="step wow animated fadeInLeft">
                            <span>01</span>
                            <h2 class="show-next">Ingresa tus datos <svg><use xlink:href="#down" /></svg></h2>
                            <p>Y selecciona con que Servicio Técnico deseas comunicarte y escríbenos cual es tu requerimiento.</p>
                        </div>
                        <div class="step wow animated fadeInLeft" style="animation-delay: .5s">
                            <h2>Servicio Técnico de Marcas</h2>
                            <h2>Servicio Técnico SUZUKI</h2>
                            <p>(Motos y Motores)</p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="vector">
                            @include('vector')
                        </div>
                    </div>
                    <div class="col">
                        <div class="step wow animated fadeInRight" style="animation-delay: .7s">
                            <span>02</span>
                            <h2 class="show-next">Nos contactaremos de inmediato contigo <svg><use xlink:href="#down" /></svg></h2>
                            <p>Responderemos tu solicitud en un plazo de máximo 24 horas para proceder a brindarte la ayuda especializada que estás buscando.</p>
                        </div>
                        <div class="step wow animated fadeInRight" style="animation-delay: 1s">
                            <span>03</span>
                            <h2 class="show-next">Atenderemos tu requerimiento <svg><use xlink:href="#down" /></svg></h2>
                            <p>Sin embargo ten en cuenta que cada fabricante tiene su propio procedimiento para el uso de los servicios de asistencia técnica.</p>
                        </div>
                    </div>
                </div>
            </section>
        
            <section class="warranty" style="background-image: url({{ asset('images/back-pattern.svg') }});">
                <div class="container">
                    <div class="item wow animated fadeInLeft"><img src="{{ asset('images/warranty.svg') }}" alt=""></div>
                    <div class="item wow animated fadeInRight">
                        <h1>Buscas una Garantía Prolongada</h1>
                        <hr>
                        <p>Todos los productos exhibidos en <b>Comandato</b> cuentan con una garantía de fábrica, pero si deseas una más prolongada tenemos para ofrecerte nuestra <b>Garantía Extendida.</b></p>
                        <address>
                            Para mayor información sobre Servicio Técnico y Repuestos llamar al:
                            <span><b>(04) 6009000</b> opción <b>3</b></span>
                        </address>
                    </div>
                </div>
            </section>

            <section class="brands">
                <div class="brands_carrousel">
                    <div class="item"><img src="{{ asset('images/brands/daewoo-logo.png') }}" alt=""></div>
                    <div class="item"><img src="{{ asset('images/brands/gemini-logo.png') }}" alt=""></div>
                    <div class="item"><img src="{{ asset('images/brands/lg-logo.png') }}" alt=""></div>
                    <div class="item"><img src="{{ asset('images/brands/philips-logo.png') }}" alt=""></div>
                    <div class="item"><img src="{{ asset('images/brands/samsung-logo.png') }}" alt=""></div>
                </div>
            </section>
        </div>

        <div class="copyright">
            <p>© 2017 Comandato | Todos los dertechos reservados.</p>
        </div>
        <script>
            var login_url = '{{ url("mail") }}';
        </script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.1.3/ofi.min.js"></script>
        <script>
            new WOW().init();
            $(function () { objectFitImages() });
        </script>
    </body>
</html>