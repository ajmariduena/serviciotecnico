<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
	@if($data['type'] == 'brands')
		<h1>Nueva Solicitud de Servicio técnico de Marcas</h1>
	@elseif($data['type'] == 'suzuki')
		<h1>Nueva Solicitud de Servicio técnico de Suzuki</h1>
	@elseif($data['type'] == 'parts')
		<h1>Nueva Solicitud de Repuestos Suzuki</h1>
	@endif

	<h3>Nombres:</h3>
	<p>{{ $data['names'] }}</p>
	<h3>Teléfono:</h3>
	<p>{{ $data['phone'] }}</p>
	<h3>Cédula:</h3>
	<p>{{ $data['legal_id'] }}</p>
	<h3>Mensaje:</h3>
	<p>{{ $data['message'] }}</p>
	
</body>
</html>