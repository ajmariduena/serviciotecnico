require('./bootstrap');

const app = new Vue({
    el: '#app',
    data: {
        type: '',
        names: '',
        phone: '',
        legal_id: '',
        message: '',
        errors: {},
        text: 'Solicitar servicio',
        success: false
    },
    methods: {
        submit() {
            this.text = 'Solicitando..';
            var self = this;
            axios.post(login_url, {
                type: this.type,
                names: this.names,
                phone: this.phone,
                legal_id: this.legal_id,
                message: this.message
            })
            .then(function (response) {
                if (response.status == 200) {
                    console.log('is equal');
                    self.text = 'Solicitar servicio';
                    self.success = true;
                }
            })
            .catch(error => {
                this.text = 'Solicitar servicio';
                this.errors = error.response.data
            });
        },
        setType(type) {
            return this.type = type;
        }
    }
});

$('.brands_carrousel').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    prevArrow: '<button type="button" class="slick-prev"><svg><use xlink:href="#left" /></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg><use xlink:href="#right" /></svg></button>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
      }
    },
      {
        breakpoint: 600,
        settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
      {
        breakpoint: 480,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
});

$('.show-next').click(function() {
    $(this).next().toggleClass('show-text');
})

$(window).scroll(function() {
    var height = $('header').height()
        scroll = $(window).scrollTop();
    if (scroll > height) {
        $('.header').addClass('header-fixed');
    } else {
        $('.header').removeClass('header-fixed');
    }
});
