<?php

Route::get('/', function () {
    return view('home');
});

Route::post('/mail', 'MailController@sendEmail')->name('mail');
