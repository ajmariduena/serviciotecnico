<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail(Request $request) 
    {
        $messages = [
            'type.required'    => 'Debes especificar con que Servicio Técnico deseas comunicarte',
            'names.required'    => 'Nombre y Apellido requerido',
            'phone.required' => 'Número de teléfono requerido',
            'legal_id.required' => 'Número de cédula requerido',
            'message.required' => 'Debes escribir un mensaje',
            'phone.numeric' => 'Debes ingresar un número de teléfono válido',
            'legal_id.numeric' => 'Debes ingresar un número de cédula válido'
        ];
    	$this->validate($request, [
	        'type' => 'required',
	        'names' => 'required',
            'phone' => 'required|numeric',
	        'legal_id' => 'required|numeric',
	        'message' => 'required'
	    ], $messages);

        $data = $request->all();

        if ($request->input('type') == 'brands') {
            Mail::send('request', ['data' => $data],function ($m) use($data) {
                $m->from('noreply@serviciotecnico.com', 'Servicio Técnico Comandato');
                $m->to('angel.zambrano@comandato.com', 'Angel Zambrano')
                ->subject('Nueva Solicitud de Servicio ténico de Marcas');
            });
        } elseif ($request->input('type') == 'suzuki') {
            Mail::send('request', ['data' => $data],function ($m) use($data) {
                $m->from('noreply@serviciotecnico.com', 'Servicio Técnico Comandato');
                $m->to('angel.zambrano@comandato.com', 'Angel Zambrano')
                ->subject('Nueva Solicitud de Servicio ténico de Suzuki');
            });
        } else if ($request->input('type') == 'parts') {
            Mail::send('request', ['data' => $data],function ($m) use($data) {
                $m->from('noreply@serviciotecnico.com', 'Servicio Técnico Comandato');
                $m->to('dsanchez@comandato.com', 'Daniel Sanchez')
                ->subject('Nueva Solicitud de Repuestos Suzuki');
            });
        } else {
            return response(['msg', 'Algo salió terriblemente mal, inténtalo más tarde'], 404)->header('Content-Type', 'application/json');
        }
        return response([], 200)->header('Content-Type', 'application/json');
    }
}
